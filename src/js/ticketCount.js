import * as Utility from './modules/utility.js';
import * as Header from './modules/header.js';
import * as Ticket from './modules/tickets.js';

document.addEventListener('DOMContentLoaded', () => {
    document.getElementById('mainHeader').appendChild(Header.create());
    document.body.appendChild(Utility.createSpinner());
    google.charts.load('current', { packages: ['corechart'] });

    Utility.showSpinner();
    Ticket.getClosedForMonth({}).then(closed => {
        let data = [["Tech", "Count"]];
        let total = 0;

        for (const [tech, count] of Object.entries(closed)) {
            data.push([Utility.getName(tech), count]);
            total += count;
        }

        document.querySelector('.container p span').textContent = total;

        google.charts.setOnLoadCallback(drawCharts(data));
    }).catch(err => {
        console.log(err);
    }).finally(() => {
        Utility.hideSpinner();
    });

    document.getElementById('dateForm').addEventListener('submit', e => {
        e.preventDefault();
        const form = e.currentTarget;
        const data = {
            month: form.querySelector('input[name="month"]').value
        }

        Utility.showSpinner();
        Ticket.getClosedForMonth(data).then(closed => {
            let data = [["Tech", "Count"]];
            let total = 0;
            Utility.clear(document.getElementById('pieChart'));
            for (const [tech, count] of Object.entries(closed)) {
                data.push([Utility.getName(tech), count]);
                total += count;
            }

            drawCharts(data);
            document.querySelector('.container p span').textContent = total;
        }).catch(err => {
            console.log(err);
        }).finally(() => {
            Utility.hideSpinner();
        });
    });
});

function drawCharts(data) {
    const chartData = google.visualization.arrayToDataTable(data);
    const pieOptions = {
        legend: {
            position: 'bottom',
            alignment: 'center',
            textStyle: {
                fontName: 'Raleway',
            }
        },
        pieSliceText: 'value',
        height: 500,
        chartArea: {
            left: 0,
            top: 10,
            right: 0,
            bottom: 50
        },
        pieHole: .5,
        pieSliceTextStyle: {
            fontName: 'Raleway'
        }
    };
    const pieChart = new google.visualization.PieChart(document.getElementById('pieChart'));
    pieChart.draw(chartData, pieOptions);

}