import * as Utility from './modules/utility.js';
import * as Header from './modules/header.js';
import * as CurrentStats from './modules/currentStats.js';
import * as Projects from './modules/projects.js';

document.addEventListener("DOMContentLoaded", () => {
    document.getElementById('mainHeader').appendChild(Header.create());
    document.body.appendChild(Utility.createSpinner());

    Utility.showSpinner();
    Promise.all([CurrentStats.getForType(1), CurrentStats.getForType(5), Projects.getYTDCount()]).then(values => {
        const stats = values[0];
        const projectCount = parseInt(values[2]);
        const companyStats = values[1];

        makeColumn(companyStats[0]);

        for (const stat of stats) {
            makeColumn(stat);
        }

        const projectMeter = document.querySelector('#projects meter');
        document.querySelector('#projects p span').textContent = projectCount;
        projectMeter.setAttribute('value', projectCount);
        projectMeter.setAttribute('max', projectCount + 2);

    }).catch(err => {
        console.log(err);
    }).finally(() => {
        Utility.hideSpinner();
    });
});



function makeColumn(data) {
    if ('content' in document.createElement('template')) {
        const baseElement = document.getElementById('techs');
        const template = document.getElementById('tech');
        const techElement = document.importNode(template.content, true);
        const photo = techElement.querySelector('img');
        let isLead = false;

        if (data.person.title === 'Operations Lead') {
            isLead = true;
        }

        if (data.person.handle === 'Sora') {
            photo.setAttribute('src', 'img/soraLogoCircle.svg');
        } else {
            photo.setAttribute('src', `img/faces/${data.person.handle}.jpg`);
        }

        if (data.person.isGeekOfTheWeek) {
            techElement.querySelector('.fa-trophy').style.display = 'block';
        }

        if (data.person.isGeekOfTheMonth) {
            techElement.querySelector('.fa-crown').style.display = 'block';
        }

        if (data.person.isGeekOfTheMonth && data.person.isGeekOfTheWeek) {
            techElement.querySelector('.awards').style["justify-content"] = 'space-between';
        }

        techElement.querySelector('p[data-name="satisfaction"]').textContent = `${Utility.checkNumber(data.stats.customerSatisfaction)}%`;
        techElement.querySelector('div[data-name="satisfaction"] i').classList.add(CurrentStats.satisfactionColor(data.stats.customerSatisfaction));

        techElement.querySelector('p[data-name="sameDay"]').textContent = `${Utility.checkNumber(data.stats.sameDay)}%`;
        techElement.querySelector('div[data-name="sameDay"] i').classList.add(CurrentStats.sameDayColor(data.stats.sameDay));

        techElement.querySelector('p[data-name="ticketTime"]').textContent = Utility.checkNumber(data.stats.ticketTime);
        techElement.querySelector('div[data-name="ticketTime"] i').classList.add(CurrentStats.ticketTimeColor(data.stats.ticketTime, isLead));

        baseElement.appendChild(techElement);
    }
}

