import * as Utility from './modules/utility.js';
import * as Header from './modules/header.js';
import * as People from './modules/people.js';

document.addEventListener('DOMContentLoaded', () => {
    document.getElementById('mainHeader').appendChild(Header.create());
    document.body.appendChild(Utility.createSpinner());

    // Utility.showSpinner();

    const params = new URLSearchParams(window.location.search);
    const id = params.get('id');

    if (id !== null) {
        document.getElementsByTagName('h1')[0].textContent = 'Edit Person';

        Utility.showSpinner();
        People.getByID(id).then(person => {
            const form = document.getElementById('personAdminForm');
            form.dataset.edit = true;
            form.dataset.personId = id;
            populateForm(form, person);
        }).catch(err => {
            console.log(err);
        }).finally(() => {
            Utility.hideSpinner();
        });
    }

    document.getElementById('personAdminForm').addEventListener('submit', e => {
        e.preventDefault();
        const form = e.currentTarget;
        const data = {
            name: form.querySelector('input[name="name"]').value,
            handle: form.querySelector('input[name="handle"]').value,
            title: form.querySelector('input[name="title"]').value,
            type: parseInt(form.querySelector('select[name="type"]').value),
            gitlabID: form.querySelector('input[name="gitlabID"]').value,
            isActive: form.querySelector('input[name="active"]').checked
        }

        Utility.showSpinner();

        if (form.hasAttribute('data-edit')) {
            data.id = form.dataset.personId;

            People.update(data).then(updated => {
                window.location.assign(`/person.html?id=${updated.id}`);
            }).catch(err => {
                console.log(err);
            }).finally(() => {
                Utility.hideSpinner();
            });

            return
        }

        People.create(data).then(created => {
            window.location.assign(`/person.html?id=${created.id}`);
        }).catch(err => {
            console.log(err);
        }).finally(() => {
            Utility.hideSpinner();
        });
    });
});

function populateForm(form, data) {
    form.querySelector('input[name="name"]').value = data.name;
    form.querySelector('input[name="handle"]').value = data.handle;
    form.querySelector('input[name="title"]').value = data.title;
    form.querySelector('select[name="type"]').value = data.type;
    form.querySelector('input[name="gitlabID"]').value = data.gitlabID;
    form.querySelector('input[name="active"]').checked = data.isActive;
}