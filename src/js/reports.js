import * as Utility from './modules/utility.js';
import * as Header from './modules/header.js';
import * as Reports from './modules/reportModule.js';
import * as Survey from './modules/surveys.js';
import * as Ticket from './modules/tickets.js';

document.addEventListener('DOMContentLoaded', () => {
    document.getElementById('mainHeader').appendChild(Header.create());
    document.body.appendChild(Utility.createSpinner());
    google.charts.load('current', { packages: ['corechart', 'bar'] });

    populateReports();

    document.getElementById('reportSelector').addEventListener('change', e => {
        showReport(e);
    });

    document.getElementById('dateRangeForm').addEventListener('submit', showReport)
});

function populatePriorities() {
    Reports.getPriorities().then(priorities => {
        for (const [name, tickets] of Object.entries(priorities)) {
            makePriorityTable(name, tickets);
        }
    }).catch(err => {
        console.log(err);
    }).finally(() => {
        Utility.hideSpinner();
    });
}

function makePriorityTable(name, tickets) {
    const el = Utility.importTemplate('priorityTemplate');
    const baseEl = document.querySelector('.container');

    el.querySelector('h2').textContent = Utility.getName(name);

    for (const ticket of tickets) {
        const tr = document.createElement('tr');
        tr.appendChild(makePriorityCell(ticket.priority.id));
        tr.appendChild(Utility.makeTextCell(ticket.id));
        tr.appendChild(Utility.makeTextCell(ticket.board.name));
        tr.appendChild(Utility.makeTextCell(ticket.company.identifier));
        tr.appendChild(Utility.makeTextCell(ticket.summary));
        tr.appendChild(Utility.makeTextCell(ticket.status.name));

        el.querySelector('table tbody').appendChild(tr);
    }

    baseEl.appendChild(el);
}

function makePriorityCell(priority) {
    const cell = document.createElement('td');
    const div = document.createElement('div');

    div.className = 'priority';
    div.dataset.priority = priority;
    cell.appendChild(div);
    return cell;
}

function populateTotalTickets(dates) {
    Reports.getTotalTickets(dates).then(totals => {
        const el = Utility.importTemplate('totalTicketTemplate');
        const tbody = el.querySelector('table tbody');
        const tfoot = el.querySelector('table tfoot tr');

        for (const [name, value] of Object.entries(totals)) {
            if (name !== 'Total') {
                const cells = [
                    name,
                    value.opened,
                    value.closed
                ];
                tbody.appendChild(Utility.makeRow(cells));
            }
        }

        tfoot.appendChild(Utility.makeTextCell(totals.Total.opened));
        tfoot.appendChild(Utility.makeTextCell(totals.Total.closed));

        document.querySelector('.container').appendChild(el);

        makeTotalTicketChart(totals);
    }).catch(err => {
        console.log(err);
    }).finally(() => {
        Utility.hideSpinner();
    });
}

function makeTotalTicketChart(totals) {
    const data = google.visualization.arrayToDataTable([
        ['Status', 'Opened', 'Closed'],
        ['User Support', totals.UserSupport.opened, totals.UserSupport.closed],
        ['Net Admin', totals.NetAdmin.opened, totals.NetAdmin.closed],
        ['Total', totals.Total.opened, totals.Total.closed]
    ]);
    const options = {
        height: 300,
        legend: {
            position: 'top'
        },
    };

    google.charts.setOnLoadCallback(() => {
        const chart = new google.visualization.ColumnChart(document.querySelector('#totalTickets .chart'));
        chart.draw(data, options);
    });
}

function populateSurveys(dates) {
    Survey.getCurrent(dates).then(surveys => {
        const el = Utility.importTemplate('surveyTemplate');
        const tbody = el.querySelector('table tbody');

        for (const survey of surveys) {
            const cells = [
                survey.ticketId,
                survey.owner.name,
                survey.company.identifier,
                survey.contact,
                survey.totalPoints,
                survey.footerResponse
            ];
            tbody.appendChild(Utility.makeRow(cells));
        }

        document.querySelector('.container').appendChild(el);
    }).catch(err => {
        console.log(err);
    }).finally(() => {
        Utility.hideSpinner();
    });
}

function populateTimeOff(dates) {
    Reports.getTimeOff(dates).then(data => {
        const el = Utility.importTemplate('timeOffTemplate');
        const tbody = el.querySelector('table tbody');

        for (const entry of data) {
            const cells = [
                Utility.getName(entry.name),
                entry.vacation,
                entry.personalTime,
                entry.holiday,
                entry.sick,
                entry.jury,
                entry.unpaid,
                entry.bereavement
            ];
            tbody.appendChild(Utility.makeRow(cells));
        }

        document.querySelector('.container').appendChild(el);
    }).catch(err => {
        console.log(err);
    }).finally(() => {
        Utility.hideSpinner();
    });
}

function populateHourly(type, dates) {
    let method;

    switch (type) {
        case "open":
            method = Ticket.getOpenHourly;
            break;
        case "closed":
            method = Ticket.getClosedHourly;
            break;
    }

    method(dates).then(tickets => {
        const el = Utility.importTemplate('hourlyTicketTemplate');
        for (const ticket of tickets) {
            const cells = [
                ticket.id,
                ticket.company.name,
                ticket.contact.name,
                ticket.actualHours,
                ticket.summary
            ];
            el.querySelector('table tbody').appendChild(Utility.makeRow(cells));
        }

        document.querySelector('.container').appendChild(el);
    }).catch(err => {
        console.log(err);
    }).finally(() => {
        Utility.hideSpinner();
    });
}

function showReport(e) {
    e.preventDefault();
    const reportSelector = document.getElementById('reportSelector');
    const dateForm = document.getElementById('dateRangeForm');
    let dates = {};
    const h1 = document.getElementsByTagName('h1')[0];
    const subtitle = document.getElementById('subtitle');
    if (e.type === 'submit') {
        dates.start = dateForm.querySelector('input[name="start"]').value;
        dates.end = dateForm.querySelector('input[name="end"]').value;
        subtitle.style.display = 'none';
    }

    if (e.type === 'change') {
        dateForm.querySelector('input[name="end"]').value = null;
        dateForm.querySelector('input[name="start"]').value = null;
        subtitle.style.display = 'block';
    }


    Utility.showSpinner();
    Utility.clear(document.querySelector('.container'));

    switch (reportSelector.value) {
        case 'priorities':
            h1.textContent = 'Daily Priorities';
            subtitle.textContent = null;
            disableDateForm(true);
            populatePriorities();
            break;
        case 'totalTickets':
            h1.textContent = 'Total Tickets';
            subtitle.textContent = 'Last Seven Days';
            disableDateForm(false);
            populateTotalTickets(dates);
            break;
        case 'surveys':
            h1.textContent = 'Surveys';
            subtitle.textContent = 'Last Seven Days';
            disableDateForm(false);
            populateSurveys(dates);
            break;
        case 'timeOff':
            h1.textContent = 'Time Off Used';
            subtitle.textContent = 'Current Calendar Year';
            disableDateForm(false);
            populateTimeOff(dates);
            break;
        case 'openHourly':
            h1.textContent = 'Open Hourly Tickets';
            subtitle.textContent = null;
            disableDateForm(true);
            populateHourly('open', dates);
            break;
        case 'closedHourly':
            h1.textContent = 'Recently Closed Hourly Tickets';
            subtitle.textContent = 'Last Seven Days';
            disableDateForm(false);
            populateHourly('closed', dates);
            break;
        case 'averageTickets':
            h1.textContent = 'Average Tickets Closed';
            subtitle.textContent = 'Last Seven Days';
            disableDateForm(false);
            populateAverageTickets(dates);
            break;
        case 'sameDay':
            h1.textContent = 'Same Day Close Percentage';
            subtitle.textContent = 'Last Seven Days';
            disableDateForm(false);
            populateSameDay(dates);
            break;
        default:
            h1.textContent = null;
            subtitle.textContent = null;
            Utility.hideSpinner();
    }
}

function disableDateForm(condition) {
    const dateForm = document.getElementById('dateRangeForm');
    const inputs = dateForm.querySelectorAll('input, button');
    for (const input of inputs) {
        if (condition) {
            input.setAttribute('disabled', 'disabled');
        } else {
            input.removeAttribute('disabled');
        }
    }
}

function populateAverageTickets(dates) {
    Ticket.getAverage(dates).then(averages => {
        const el = Utility.importTemplate('chartTemplate');
        let data = [['Technician', 'Average', { role: 'annotation' }]];
        const options = {
            height: 500,
            fontName: 'Raleway',
            legend: 'none',
            chartArea: {
                left: 50,
                right: 0,
                top: 10,
            }
        };
        for (const tech of averages) {
            if (tech.handle !== '') {
                data.push([Utility.getName(tech.handle), tech.score, tech.score]);
            }
        }
        const chartData = google.visualization.arrayToDataTable(data);
        document.querySelector('.container').appendChild(el);

        google.charts.setOnLoadCallback(() => {
            const chart = new google.visualization.ColumnChart(document.querySelector('.chart'));
            chart.draw(chartData, options);
        });

    }).catch(err => {
        console.log(err);
    }).finally(() => {
        Utility.hideSpinner();
    });
}

function populateSameDay(dates) {
    Ticket.getSameDay(dates).then(percentages => {
        const el = Utility.importTemplate('chartTemplate');
        let data = [['Technician', 'Percentage', { role: 'annotation' }]];
        const options = {
            height: 500,
            fontName: 'Raleway',
            legend: 'none',
            chartArea: {
                left: 50,
                right: 0,
                top: 10,
            }
        };
        for (const tech of percentages) {
            if (tech.handle != '') {
                data.push([Utility.getName(tech.handle), tech.score, `${tech.score.toFixed(2)}%`]);
            }
        }
        const chartData = google.visualization.arrayToDataTable(data);
        document.querySelector('.container').appendChild(el);

        google.charts.setOnLoadCallback(() => {
            const chart = new google.visualization.ColumnChart(document.querySelector('.chart'));
            chart.draw(chartData, options);
        });

    }).catch(err => {
        console.log(err);
    }).finally(() => {
        Utility.hideSpinner();
    });
}

function populateReports() {
    const selector = document.getElementById('reportSelector');
    const restrictedReports = [
        {
            name: 'Total Tickets',
            value: 'totalTickets',
            permissions: 'grpStatusBoardAdmin'
        },
        {
            name: 'Surveys',
            value: 'surveys',
            permission: 'grpStatusBoardAdmin'
        },
        {
            name: 'Open Hourly Tickets',
            value: 'openHourly',
            permission: 'grpStatusBoardAdmin'
        },
        {
            name: 'Recently Closed Hourly Tickets',
            value: 'closedHourly',
            permission: 'grpStatusBoardAdmin'
        },
        {
            name: 'Time Off',
            value: 'timeOff',
            permission: 'grpStatusBoardAdmin'
        }
    ]

    for (const report of restrictedReports) {
        if (Utility.inGroup(report.permission)) {
            selector.appendChild(makeOption(report));
        }
    }
}

function makeOption(data) {
    const option = document.createElement('option');
    option.value = data.value;
    option.appendChild(document.createTextNode(data.name));
    return option;
}

