import * as Utility from './modules/utility.js';
import * as Header from './modules/header.js';

document.addEventListener('DOMContentLoaded', () => {
    document.getElementById('mainHeader').appendChild(Header.create());
    document.body.appendChild(Utility.createSpinner());

    document.getElementById('loginForm').addEventListener('submit', e => {
        e.preventDefault();
        const form = e.currentTarget;
        const data = {
            username: form.querySelector('input[name="username"]').value,
            password: form.querySelector('input[name="password"]').value,
        }

        login(data);
    });
});

async function login(data) {
    const response = await fetch(`${Utility.api()}/login`, {
        method: 'POST',
        credentials: 'include',
        body: JSON.stringify(data)
    });

    if (response.ok) {
        const data = await response.json();
        window.sessionStorage.setItem("id", data.id);
        window.sessionStorage.setItem("username", data.username);
        window.sessionStorage.setItem("groups", data.groups.join(', '));
        window.location.assign('/');
        return
    }

    const err = await response.text();
    throw new Error(err);
}