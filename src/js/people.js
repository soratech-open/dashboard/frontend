import * as Utility from './modules/utility.js';
import * as Header from './modules/header.js';
import * as People from './modules/people.js';

document.addEventListener("DOMContentLoaded", () => {
    document.getElementById('mainHeader').appendChild(Header.create());
    document.body.appendChild(Utility.createSpinner());

    if (Utility.inGroup('grpStatusBoardAdmin')) {
        document.querySelector('main header a').style.display = 'block';
    }

    Utility.showSpinner();
    People.getActive().then(people => {
        for (const person of people) {
            makePerson(person);
        }
    }).catch(err => {
        console.log(err);
    }).finally(() => {
        Utility.hideSpinner();
    });
});

function makePerson(data) {
    if ('content' in document.createElement('template')) {
        const baseElement = document.getElementById('people');
        const template = document.getElementById('personTemplate');
        const personElement = document.importNode(template.content, true);
        const photo = personElement.querySelector('img');

        personElement.querySelector('a').href = `/person.html?id=${data.id}`;

        photo.src = `img/faces/${data.handle}.jpg`;
        photo.setAttribute('alt', data.name);

        personElement.querySelector('p.name').textContent = data.name;
        personElement.querySelector('p.title').textContent = data.title;

        baseElement.appendChild(personElement);

    }
}