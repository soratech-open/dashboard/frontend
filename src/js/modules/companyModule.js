import * as Utility from './utility.js';

export async function getByStatus(status) {
    const response = await fetch(`${Utility.api()}/companies/status?status=${status}`, {
        method: 'GET',
        credentials: 'include'
    });

    if (response.ok) {
        return await response.json();
    }

    const err = await response.text();
    throw new Error(err);
}

export async function getByID(id) {
    const response = await fetch(`${Utility.api()}/companies/id?id=${id}`, {
        method: 'GET',
        credentials: 'include'
    });

    if (response.ok) {
        return await response.json();
    }

    const err = await response.text();
    throw new Error(err);
}