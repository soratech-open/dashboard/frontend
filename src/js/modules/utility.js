export function api() {
    switch (window.location.hostname) {
        case 'localhost':
            return 'http://localhost:32401/api';
    }
}

export function createSpinner() {
    const overlay = document.createElement('aside');
    const div = document.createElement('div');
    div.id = 'spinner';
    overlay.id = 'loadingOverlay';
    overlay.appendChild(div);
    return overlay;
}

export function showSpinner() {
    document.getElementById('loadingOverlay').style.display = 'flex';
}

export function hideSpinner() {
    document.getElementById('loadingOverlay').style.display = 'none';
}

export function isAuthed() {
    if (window.sessionStorage.getItem('id') === null) {
        return false;
    }

    return true;
}

export function inGroup(group) {
    if (isAuthed()) {
        let groups = window.sessionStorage.getItem('groups');
        groups = groups.split(", ");

        return groups.includes(group);

    }

    return false;
}

export function isUser(id) {
    if (isAuthed()) {
        const loggedID = window.sessionStorage.getItem('id');

        return loggedID === id;
    }

    return false;
}

export function checkNumber(value) {
    let displayValue;
    switch (value) {
        case undefined:
        case NaN:
            displayValue = 0;
            break;
        default:
            displayValue = parseFloat(value);
    }

    return displayValue.toFixed(2);
}

export function getBoardName(id) {
    const boards = {
        1: 'User Support',
        11: 'Proactive',
        13: 'Net Admin'
    };

    return boards[id];
}

export function makeTextCell(text) {
    const cell = document.createElement('td');
    cell.appendChild(document.createTextNode(text));
    return cell;
}

export function getName(handle) {
    const names = {
        aritschel: 'Andrew Ritschel',
        asnell: 'Andrew Snell',
        dries: 'Deven Ries',
        jtrout1: 'Jerad Trout',
        jcruse: 'Jordan Cruse',
        mkistler: 'Michelle Kistler',
        tduffy: 'Tebin Duffy',
        tstark: 'Todd Stark'
    }

    if (!names.hasOwnProperty(handle.toLowerCase())) {
        return handle;
    }

    return names[handle.toLowerCase()];
}

export function clear(el) {
    while (el.firstChild) {
        el.firstChild.remove();
    }
}

export function formatPhoneNumber(number) {
    const areaCode = number.substring(0, 3);
    const prefix = number.substring(3, 6);
    const end = number.substring(6, 10);
    return `(${areaCode}) ${prefix}-${end}`;
}

export function importTemplate(name) {
    if ('content' in document.createElement('template')) {
        const template = document.getElementById(name);
        const el = document.importNode(template.content, true);
        return el;
    }

    return false;
}

export function makeRow(data) {
    const tr = document.createElement('tr');
    for (const cell of data) {
        tr.appendChild(makeTextCell(cell));
    }
    return tr;
}

export function getMonthName(number) {
    const months = {
        1: 'Jan',
        2: 'Feb',
        3: 'Mar',
        4: 'Apr',
        5: 'May',
        6: 'Jun',
        7: 'Jul',
        8: 'Aug',
        9: 'Sep',
        10: 'Oct',
        11: 'Nov',
        12: 'Dec'
    }

    return months[number];
}

export function addParamsToUrl(data, path) {
    const url = new URL(`${api()}${path}`);
    let params = new URLSearchParams();

    if (Object.getOwnPropertyNames(data).length > 0) {
        for (const [key, value] of Object.entries(data)) {
            params.append(key, value);
        }

        url.search = params.toString();
    }

    return url.toString();
}