import * as Utility from './utility.js';

export function create() {
    const div = document.createElement('div');

    // create logo link
    const logoLink = document.createElement('a');
    const logoImg = new Image;
    logoImg.src = 'img/soraLogoWithWhiteText.svg';
    logoImg.alt = "Sora Technologies Logo";
    logoLink.setAttribute('href', '/');
    logoLink.appendChild(logoImg);

    div.appendChild(logoLink);

    // create nav block
    const nav = document.createElement('nav');

    for (const link of links) {
        const a = document.createElement('a');
        a.setAttribute('href', link.href);
        a.textContent = link.name;

        for (const [attrName, attrValue] of Object.entries(link.attributes)) {
            a.setAttribute(attrName, attrValue);
        }

        if (link.permission === undefined) {
            nav.appendChild(a);
        } else {
            if (Utility.inGroup(link.permission)) {
                nav.appendChild(a);
            }
        }
    }

    // create login/logout link
    const logoutLink = document.createElement('a');
    logoutLink.id = 'loginLink';


    if (window.sessionStorage.getItem("id") !== null) {
        logoutLink.dataset.authenticated = true;
        logoutLink.textContent = 'Logout';
        logoutLink.href = '#';
    } else {
        logoutLink.dataset.authenticated = false;
        logoutLink.textContent = 'Login';
        logoutLink.href = '/login.html';
    }

    logoutLink.addEventListener('click', e => {
        e.preventDefault();

        if (JSON.parse(logoutLink.dataset.authenticated)) {
            logout();
            return
        }

        window.location.assign(logoutLink.href);
    });

    nav.appendChild(logoutLink);

    div.appendChild(nav);

    return div;
}

async function logout() {
    const response = await fetch(`${Utility.api()}/login`, {
        method: 'DELETE',
        credentials: 'include'
    });

    if (response.ok) {
        window.sessionStorage.clear();
        window.location.assign('/');
        return
    }

    const err = await response.text();
    throw new Error(err);
}

// links defines which links should appear in the nav.
const links = [
    {
        name: "Home",
        href: "/",
        attributes: []
    },
    {
        name: "People",
        href: "/people.html",
        attributes: []
    },
    {
        name: "Reports",
        href: "/reports.html",
        attributes: []
    },
    {
        name: "Ticket Count",
        href: "/ticketCount.html",
        attributes: []
    },
    {
        name: "YTD Stats",
        href: '/ytdStats.html',
        attributes: []
    },
    {
        name: "Geek of the Week",
        href: "/geekOfTheWeek.html",
        attributes: [],
        permission: "grpStatusBoardAdmin"
    },

]