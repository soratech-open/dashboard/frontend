import * as Utility from './utility.js';

export async function getCountFor(data) {
    const url = Utility.addParamsToUrl(data, '/tickets/count/person')
    const response = await fetch(url, {
        method: 'GET',
        credentials: 'include'
    });

    if (response.ok) {
        return await response.json();
    }

    const err = await response.text();
    throw new Error(err);
}

export async function getOpenHourly() {
    const response = await fetch(`${Utility.api()}/tickets/hourly/open`, {
        method: 'GET',
        credentials: 'include'
    });

    if (response.ok) {
        return await response.json();
    }

    const err = await response.text();
    throw new Error(err);
}

export async function getClosedHourly(dates) {
    const url = Utility.addParamsToUrl(dates, '/tickets/hourly/closed');
    const response = await fetch(url, {
        method: 'GET',
        credentials: 'include'
    });

    if (response.ok) {
        return await response.json();
    }

    const err = await response.text();
    throw new Error(err);
}

export async function getClosedForMonth(data) {
    const url = Utility.addParamsToUrl(data, '/tickets/count/month')
    const response = await fetch(url, {
        method: 'GET',
        credentials: 'include'
    });

    if (response.ok) {
        return await response.json();
    }

    const err = await response.text();
    throw new Error(err);
}

export async function getAverage(dates) {
    const url = Utility.addParamsToUrl(dates, '/tickets/average');
    const response = await fetch(url, {
        method: 'GET',
        credentials: 'include'
    });

    if (response.ok) {
        return await response.json();
    }

    const err = await response.text();
    throw new Error(err);
}

export async function getSameDay(dates) {
    const url = Utility.addParamsToUrl(dates, '/tickets/same-day');
    const response = await fetch(url, {
        method: 'GET',
        credentials: 'include'
    });

    if (response.ok) {
        return await response.json();
    }

    const err = await response.text();
    throw new Error(err);
}