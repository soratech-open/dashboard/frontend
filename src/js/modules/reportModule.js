import * as Utility from './utility.js';

export async function getPriorities() {
    const response = await fetch(`${Utility.api()}/reports/priorities`, {
        method: 'GET',
        credentials: 'include'
    });

    if (response.ok) {
        return await response.json();
    }

    const err = await response.text();
    throw new Error(err);
}

export async function getTotalTickets(dates) {
    const url = Utility.addParamsToUrl(dates, '/reports/total-tickets');
    const response = await fetch(url.toString(), {
        method: 'GET',
        credentials: 'include'
    });

    if (response.ok) {
        return await response.json();
    }

    const err = await response.text();
    throw new Error(err);
}

export async function getTimeOff(dates) {
    const url = Utility.addParamsToUrl(dates, '/reports/time-off');
    const response = await fetch(url, {
        method: 'GET',
        credentials: 'include'
    });

    if (response.ok) {
        return await response.json();
    }

    const err = await response.text();
    throw new Error(err);
}