class StatMeter extends HTMLElement {
    static get observedAttributes() {
        return ["value"]
    }

    attributeChangedCallback(name, oldValue, newValue) {
        switch (name) {
            case 'value':
                const value = parseFloat(newValue);
                this.shadowRoot.querySelector('meter').setAttribute('value', newValue);

                if (this.hasAttribute('percentage')) {
                    this.shadowRoot.querySelector('span').textContent = `${value.toFixed(2)}%`;
                } else {
                    this.shadowRoot.querySelector('span').textContent = value.toFixed(2);
                }
                break;
        }
    }

    typeValues(type) {
        let values = {};
        switch (type) {
            case 'satisfaction':
                values.min = 0;
                values.max = 100;
                values.optimum = 100;
                values.low = 92;
                values.high = 97;
                break;
            case 'sameDay':
                values.min = 0;
                values.max = 100;
                values.optimum = 100;
                values.low = 75;
                values.high = 90;
                break;
            case 'ticketsClosed':
                values.min = 0;
                values.max = 10;
                values.optimum = 10;
                values.low = 5;
                values.high = 8;
                break;
            case 'ticketTime':
                values.min = 0;
                values.max = 60;
                values.optimum = 60;
                values.low = 31;
                values.high = 51;
                break;
        }
        return values;
    }

    constructor() {
        super();

        const shadow = this.attachShadow({ mode: 'open' });
        const div = document.createElement('div');
        const span = document.createElement('span');
        const meter = document.createElement('meter');
        const type = this.getAttribute('type');
        const attributes = Object.entries(this.typeValues(type));
        const style = document.createElement('style');
        const value = parseFloat(this.getAttribute('value'));

        style.textContent = `
            div {
                position:relative;
                width:100%;
            }

            span {
                position:absolute;
                left:5px;
                top:50%;
                transform:translateY(-50%);
                font-size:0.8em;
            }

            meter {
                width:100%;
            }

            meter.inverted::-webkit-meter-optimum-value {
                background:linear-gradient(rgb(255, 119, 119), rgb(255, 204, 204) 20%, rgb(221, 68, 68) 45%, rgb(221, 68, 68) 55%, rgb(255, 119, 119));
            }

            meter.inverted::-webkit-meter-even-less-good-value {
                background:linear-gradient(rgb(170, 221, 119), rgb(204, 238, 170) 20%, rgb(119, 170, 51) 45%, rgb(119, 170, 51) 55%, rgb(170, 221, 119));
            }

            meter.inverted:-moz-meter-sub-sub-optimum::-moz-meter-bar {
                background:#28cd41;
            }
            
            meter.inverted:-moz-meter-optimum::-moz-meter-bar {
                background:#ff3b30
            }
        `;

        if (this.hasAttribute('inverted')) {
            meter.className = 'inverted';
        }

        meter.setAttribute('value', value.toFixed(2));

        for (const [key, value] of attributes) {
            meter.setAttribute(key, value);
        }

        div.appendChild(span);
        div.appendChild(meter);

        shadow.appendChild(style);
        shadow.appendChild(div);
    }




}

document.addEventListener('DOMContentLoaded', () => {
    customElements.define('stat-meter', StatMeter);
});

