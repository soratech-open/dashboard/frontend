import * as Utility from './utility.js';

export async function getForPersonYTD(id) {
    const response = await fetch(`${Utility.api()}/stats/person?id=${id}`, {
        method: 'GET',
        credentials: 'include'
    });

    if (response.ok) {
        return await response.json();
    }

    const err = await response.text();
    throw new Error(err);
}

export async function getAllYTD() {
    const response = await fetch(`${Utility.api()}/stats/all`, {
        method: 'GET',
        credentials: 'include'
    });

    if (response.ok) {
        return await response.json();
    }

    const err = await response.text();
    throw new Error(err);
}