import * as Utility from './utility.js';

export async function getForPerson(id) {
    const response = await fetch(`${Utility.api()}/award?id=${id}`, {
        method: 'GET',
        credentials: 'include'
    });

    if (response.ok) {
        return await response.json();
    }

    const err = await response.text();
    throw new Error(err);
}

export async function create(data) {
    const response = await fetch(`${Utility.api()}/award`, {
        credentials : 'include',
        method : 'POST',
        body : JSON.stringify(data)
    });

    if (response.ok) {
        return await response.json();
    }

    const err = await response.text();
    throw new Error(err);
}