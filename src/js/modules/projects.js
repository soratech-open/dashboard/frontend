import * as Utility from './utility.js';

export async function getYTDCount() {
    const response = await fetch(`${Utility.api()}/project/ytd`, {
        method: 'GET',
        credentials: 'include'
    });

    if (response.ok) {
        return await response.json();
    }

    const err = await response.text();
    throw new Error(err);
}