import * as Utility from './utility.js';

export async function getActive() {
    const response = await fetch(`${Utility.api()}/people/active`, {
        method: 'GET',
        credentials: 'include'
    });

    if (response.ok) {
        return await response.json();
    }

    const err = await response.text();
    throw new Error(err);
}

export async function getByID(id) {
    const response = await fetch(`${Utility.api()}/people/person/id?id=${id}`, {
        method: 'GET',
        credentials: 'include'
    });

    if (response.ok) {
        return await response.json();
    }

    const err = await response.text();
    throw new Error(err);
}

export async function create(data) {
    const response = await fetch(`${Utility.api()}/people/create`, {
        method: "POST",
        credentials: "include",
        body: JSON.stringify(data)
    });

    if (response.ok) {
        return await response.json();
    }

    const err = await response.text();
    throw new Error(err);
}

export async function update(data) {
    const response = await fetch(`${Utility.api()}/people/update`, {
        method: "PUT",
        credentials: "include",
        body: JSON.stringify(data)
    });

    if (response.ok) {
        return await response.json();
    }

    const err = await response.text();
    throw new Error(err);
}