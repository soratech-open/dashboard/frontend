import * as Utility from './utility.js';

export async function getCurrentFor(name) {
    const response = await fetch(`${Utility.api()}/surveys/person?handle=${name}`, {
        credentials: 'include',
        method: 'GET'
    });

    if (response.ok) {
        return await response.json();
    }

    const err = await response.text();
    throw new Error(err);
}

export async function getCurrent(dates) {
    const url = Utility.addParamsToUrl(dates, '/surveys/all');
    const response = await fetch(url, {
        credentials: 'include',
        method: 'GET'
    });

    if (response.ok) {
        return await response.json();
    }

    const err = await response.text();
    throw new Error(err);
}

export async function getForPersonInRange(data) {
    const url = Utility.addParamsToUrl(data, '/surveys/person');
    const response = await fetch(url, {
        credentials: 'include',
        method: 'GET'
    });

    if (response.ok) {
        return await response.json();
    }

    const err = await response.text();
    throw new Error(err);
}