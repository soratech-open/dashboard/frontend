import * as Utility from './utility.js';

export async function getForType(type) {
    const response = await fetch(`${Utility.api()}/current-stats/type?type=${type}`, {
        method: 'GET',
        credentials: 'include'
    });

    if (response.ok) {
        return await response.json();
    }

    const err = await response.text();
    throw new Error(err);
}

export async function getForPerson(id) {
    const response = await fetch(`${Utility.api()}/current-stats/person?id=${id}`, {
        method: 'GET',
        credentials: 'include'
    });

    if (response.ok) {
        return await response.json();
    }

    const err = await response.text();
    throw new Error(err);
}

export function satisfactionColor(value) {
    const intVal = parseInt(value);
    switch (true) {
        case intVal < 92:
            return 'red';
        case intVal >= 92 && intVal < 97:
            return 'yellow';
        case intVal >= 97:
            return 'green';
        default:
            return 'red';
    }
}

export function sameDayColor(value) {
    const intVal = parseInt(value);
    switch (true) {
        case intVal < 75:
            return 'red';
        case intVal >= 75 && intVal < 90:
            return 'yellow';
        case intVal >= 90:
            return 'green';
        default:
            return 'red';
    }
}

export function ticketTimeColor(value, isLead) {
    const intVal = parseInt(value);
    let green = 30;
    let yellow = 50;
    if (isLead) {
        green = 60;
        yellow = 120;
    }
    switch (true) {
        case intVal <= green:
            return 'green';
        case intVal > green && intVal <= yellow:
            return 'yellow';
        case intVal > yellow:
        default:
            return 'red';
    }
}