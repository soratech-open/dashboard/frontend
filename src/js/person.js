import * as Utility from './modules/utility.js';
import * as Header from './modules/header.js';
import * as CurrentStats from './modules/currentStats.js';
import * as People from './modules/people.js';
import * as Award from './modules/awards.js';
import * as Stats from './modules/monthlyStats.js';
import * as Tickets from './modules/tickets.js';
import * as Surveys from './modules/surveys.js';

document.addEventListener('DOMContentLoaded', () => {
    document.getElementById('mainHeader').appendChild(Header.create());
    document.body.appendChild(Utility.createSpinner());
    google.charts.load('current', { packages: ['corechart'] });

    const params = new URLSearchParams(window.location.search);
    const id = params.get('id');
    const awardForm = document.getElementById('awardForm');
    const loadSurveyButton = document.getElementById('loadSurveyButton');

    document.getElementById('editPersonButton').addEventListener('click', () => {
        window.location.assign(`/person-admin.html?id=${id}`);
    });

    document.getElementById('cancelButton').addEventListener('click', clearAwardForm);

    document.getElementById('addAwardButton').addEventListener('click', () => {
        awardForm.style.display = 'grid';
    });

    awardForm.dataset.personId = id;
    awardForm.addEventListener('submit', awardHandler);

    if (Utility.isUser(id) || Utility.inGroup('grpStatusBoardAdmin')) {
        document.getElementById('surveys').style.display = 'block';
    }

    if (Utility.inGroup('grpStatusBoardAdmin')) {
        document.getElementById('buttons').style.display = 'flex';
    }


    Utility.showSpinner();
    (async () => {
        // get the person's data
        let person;

        try {
            person = await People.getByID(id);
            populatePerson(person);
            document.getElementById('surveyRangeForm').dataset.handle = person.handle;
        } catch (error) {
            console.log(error);
        }

        // if the person is a tech, get their current stats.
        if (person.type === 1) {
            try {
                const stats = await CurrentStats.getForPerson(id);
                populateCurrentStats(stats);
                document.getElementById('currentStats').style.display = 'block';
            } catch (error) {
                console.log(error);
            }

            // get the tech's year to date stats.
            try {
                const monthStats = await Stats.getForPersonYTD(id);
                for (const stat of monthStats) {
                    populateMonthStatsFromTemplate(stat);
                }

                document.getElementById('ytdStats').style.display = 'block';

            } catch (error) {
                console.log(error);
            }

            // get the tech's ticket close numbers
            const data = {
                handle: person.handle
            }
            populateTicketCount(data);
        }

        // if the person is a tech or a dev, get their awards.
        if (person.type === 1 || person.type === 2) {
            try {
                const awards = await Award.getForPerson(id);
                if (awards != null) {
                    for (const award of awards) {
                        populateAwards(award);
                    }
                    document.getElementById('awards').style.display = 'block';
                }
            } catch (error) {
                console.log(error);
            }
        }

        Utility.hideSpinner();
    })();

    document.getElementById('surveyRangeForm').addEventListener('submit', e => {
        e.preventDefault();
        const form = e.currentTarget;
        const start = form.querySelector('input[name="start"]').value;
        const end = form.querySelector('input[name="end"]').value;
        let data = {
            handle: form.dataset.handle
        };

        if (start !== '') {
            data.start = start;
        }

        if (end !== '') {
            data.end = end;
        }

        Utility.showSpinner();
        Surveys.getForPersonInRange(data).then(results => {
            Utility.clear(document.querySelector('#surveyTable tbody'));
            for (const result of results) {
                displaySurvey(result);
            }
        }).catch(err => {
            console.log(err);
        }).finally(() => {
            Utility.hideSpinner();
        });
    });
});

function populatePerson(data) {
    const photo = document.querySelector('main header img');
    photo.src = `img/faces/${data.handle}.jpg`
    photo.alt = data.name;

    document.querySelector('main header .data h1').textContent = data.name;
    document.querySelector('main header .data .title').textContent = data.title;
}

function populateCurrentStats(data) {
    const satisfactionDiv = document.querySelector('#currentStats div[data-name="customerSatisfaction"]');
    const sameDayDiv = document.querySelector('#currentStats div[data-name="sameDay"]');
    const ticketTimeDiv = document.querySelector('#currentStats div[data-name="ticketTime"]');
    let isLead = false;

    if (data.person.title === 'Operations Lead') {
        isLead = true;
    }

    satisfactionDiv.querySelector('p').textContent = `${Utility.checkNumber(data.stats.customerSatisfaction)}%`;
    satisfactionDiv.querySelector('i').classList.add(CurrentStats.satisfactionColor(data.stats.customerSatisfaction));

    sameDayDiv.querySelector('p').textContent = `${Utility.checkNumber(data.stats.sameDay)}%`;
    sameDayDiv.querySelector('i').classList.add(CurrentStats.sameDayColor(data.stats.sameDay));

    ticketTimeDiv.querySelector('p').textContent = Utility.checkNumber(data.stats.ticketTime);
    ticketTimeDiv.querySelector('i').classList.add(CurrentStats.ticketTimeColor(data.stats.ticketTime, isLead));
}

function populateAwards(data) {
    if ('content' in document.createElement('template')) {
        const baseElement = document.querySelector('#awards .container');
        const template = document.getElementById('awardTemplate');
        const awardElement = document.importNode(template.content, true);
        const awardIcon = awardElement.querySelector('.fas');
        let awardClass;

        switch (data.name) {
            case 'Geek of the Week':
                awardClass = 'fa-trophy';
                break;
            case 'Geek of the Month':
                awardClass = 'fa-crown';
                awardElement.querySelector('div').classList.add('gotm');

        }

        awardIcon.classList.add(awardClass);

        awardElement.querySelector('.name').textContent = data.name;
        awardElement.querySelector('.date').textContent = data.date;

        baseElement.appendChild(awardElement);
    }
}

function populateMonthStatsFromTemplate(data) {
    if ('content' in document.createElement('template')) {
        const months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        const statDate = new Date(data.date);
        const statMonth = statDate.getMonth();
        const baseElement = document.querySelector('#ytdStats .container');
        const template = document.getElementById('monthStatTemplate');
        const el = document.importNode(template.content, true);

        el.querySelector('.month').textContent = months[statMonth];
        el.querySelector('stat-meter[type="satisfaction"]').setAttribute('value', data.stats.customerSatisfaction);
        el.querySelector('stat-meter[type="sameDay"]').setAttribute('value', data.stats.sameDay);
        el.querySelector('stat-meter[type="ticketsClosed"]').setAttribute('value', data.stats.averageTickets);
        el.querySelector('stat-meter[type="ticketTime"]').setAttribute('value', data.stats.ticketTime);

        baseElement.appendChild(el);

    }
}

function awardHandler(e) {
    e.preventDefault();
    const form = e.currentTarget;
    const awardSection = document.getElementById('awards');
    const data = {
        name: form.querySelector('select[name="name"]').value,
        person: form.dataset.personId
    }

    Utility.showSpinner();

    Award.create(data).then(created => {
        populateAwards(created);
        awardSection.style.display = 'block';
        clearAwardForm();
    }).catch(err => {
        console.log(err);
    }).finally(() => {
        Utility.hideSpinner()
    });
}

function clearAwardForm() {
    const form = document.getElementById('awardForm');
    form.style.display = 'none';
    form.querySelector('select').value = '';

}

function drawChart(data) {
    google.charts.setOnLoadCallback(() => {
        const dataTable = google.visualization.arrayToDataTable(data);
        const chart = new google.visualization.PieChart(document.getElementById('chart'));
        const chartOptions = {
            'height': 400,
            'pieSliceText': 'value',
            'legend': {
                position: 'bottom',
                alignment: 'center'
            },
            chartArea: {
                top: 10,
                right: 0,
                bottom: 50,
                left: 0
            }
        };

        chart.draw(dataTable, chartOptions);
    });
}

function displaySurvey(data) {
    if (data !== null) {
        const surveyTable = document.querySelector('#surveyTable tbody');
        const tr = document.createElement('tr');
        tr.appendChild(Utility.makeTextCell(data.ticketId));
        tr.appendChild(Utility.makeTextCell(data.company.identifier));
        tr.appendChild(Utility.makeTextCell(data.contact));
        tr.appendChild(Utility.makeTextCell(data.totalPoints));
        tr.appendChild(Utility.makeTextCell(data.footerResponse));

        surveyTable.appendChild(tr);
    }
}

function populateTicketCount(params) {
    let data = [
        ['Board', 'Closed']
    ];
    let total = 0;
    const totalDisplay = document.querySelector('#ticketCount div p');
    Tickets.getCountFor(params).then(counts => {
        for (const count of counts) {
            let tableData = [Utility.getBoardName(count.Board), count.Count];
            data.push(tableData);
            total += count.Count;
        }
        totalDisplay.appendChild(document.createTextNode(total));
        drawChart(data);
        document.getElementById('ticketCount').style.display = 'block';
    }).catch(err => {
        console.log(err);
    });

}
