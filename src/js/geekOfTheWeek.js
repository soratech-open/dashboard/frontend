import * as Utility from './modules/utility.js';
import * as Header from './modules/header.js';
import * as People from './modules/people.js';
import * as Award from './modules/awards.js';

document.addEventListener('DOMContentLoaded', e => {
    document.getElementById('mainHeader').appendChild(Header.create());
    document.body.appendChild(Utility.createSpinner());

    Utility.showSpinner();
    People.getActive().then(people => {
        const container = document.querySelector('.container');
        const types = [1, 2];
        for (const person of people) {
            if (types.includes(person.type) && person.title !== 'Operations Lead') {
                container.appendChild(makePerson(person));
            }
        }
    }).catch(err => {
        console.log(err);
    }).finally(() => {
        Utility.hideSpinner();
    });

    document.getElementById('pickWinnerButton').addEventListener('click', () => {
        const people = document.getElementsByClassName('person');
        const currentWinner = document.querySelector('.winner');
        if (currentWinner !== null) {
            currentWinner.classList.remove('winner');
        }
        const random = Math.floor((Math.random() * people.length));
        people[random].classList.add('winner');
    });

    document.getElementById('confirmButton').addEventListener('click', () => {
        const winner = document.querySelector('.winner');

        if (winner !== null) {
            Utility.showSpinner();

            const data = {
                person: winner.dataset.id,
                name: 'Geek of the Week',
            }

            Award.create(data).then(result => {
                console.log(result);
            }).catch(err => {
                console.log(err);
            }).finally(() => {
                Utility.hideSpinner();
            });
        }
    });
});

function makePerson(person) {
    const el = Utility.importTemplate('personTemplate');
    el.querySelector('img').setAttribute('src', `img/faces/${person.handle}.jpg`);
    el.querySelector('p').textContent = person.name;
    el.querySelector('div').dataset.id = person.id;
    el.querySelector('button').addEventListener('click', e => {
        const parent = e.currentTarget.parentElement;
        parent.remove();
    });

    return el;
}