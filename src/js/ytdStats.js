import * as Utility from './modules/utility.js';
import * as Header from './modules/header.js';
import * as Stats from './modules/monthlyStats.js';

document.addEventListener('DOMContentLoaded', () => {
    document.getElementById('mainHeader').appendChild(Header.create());
    document.body.appendChild(Utility.createSpinner());

    Utility.showSpinner();
    Stats.getAllYTD().then(stats => {
        document.querySelector('main').appendChild(makeTechRow('sora', stats.sora));
        delete stats.sora;

        for (const [name, data] of Object.entries(stats)) {
            document.querySelector('main').appendChild(makeTechRow(name, data));
        }

    }).catch(err => {
        console.log(err);
    }).finally(() => {
        Utility.hideSpinner();
    });
});

function makeTechRow(name, stats) {
    const techEl = Utility.importTemplate('techStatTemplate');
    const section = techEl.querySelector('.stats');

    if (name === 'sora') {
        techEl.querySelector('img').src = `img/soraLogoCircle.svg`;
    } else {
        techEl.querySelector('img').src = `img/faces/${name}.jpg`;
    }

    for (let i = 1; i < 13; i++) {
        const monthEl = Utility.importTemplate('monthStatTemplate');
        monthEl.querySelector('.month').dataset.month = i;
        monthEl.querySelector('.label').textContent = Utility.getMonthName(i);
        section.appendChild(monthEl);
    }

    for (const stat of stats) {
        const statMonth = new Date(stat.date).getMonth() + 1;
        const statEl = section.querySelector(`.month[data-month="${statMonth}"]`);
        statEl.querySelector('stat-meter[type="satisfaction"]').setAttribute('value', stat.stats.customerSatisfaction);
        statEl.querySelector('stat-meter[type="sameDay"]').setAttribute('value', stat.stats.sameDay);
        statEl.querySelector('stat-meter[type="ticketsClosed"]').setAttribute('value', stat.stats.averageTickets);
        statEl.querySelector('stat-meter[type="ticketTime"]').setAttribute('value', stat.stats.ticketTime);


    }

    return techEl;
}